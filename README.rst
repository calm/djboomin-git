djboomin
========

DJ-Boomin is a reskin of the django admin using Twitter Bootstrap. It is based on strapmin built by Tom Carrick.

The name comes from DJango BOOtstrap adMIN.

It's fully responsive and includes CKEditor for easy editing of HTML.


Installation
------------

1. `pip install djboomin`
2. Add request to your context processors::

        from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
        TEMPLATE_CONTEXT_PROCESSORS = TCP + ('django.core.context_processors.request',)

3. Add 'djboomin' to your INSTALLED_APPS::

        INSTALLED_APPS = (
            'djboomin',
            'django.contrib.admin',
            ...
        )

Note: You must add `djboomin` *before* `django.contrib.admin`



Usage
-----

Create your `ModelAdmin` classes as normal.


CKEditor
~~~~~~~~

To include a CKEditor instance for all TextFields in a model::

    from django.contrib import admin
    from django.db import models

    from djboomin.widgets import RichTextEditorWidget

    from polls.models import Poll


    class ArticleAdmin(admin.ModelAdmin):
        formfield_overrides = {models.TextField: {'widget': RichTextEditorWidget}}

For a subset of models, make a form for your model that uses the widget::

    from django import forms

    from djboomin.widgets import RichTextEditorWidget

    from polls.models import Poll


    class PollForm(forms.ModelForm):
        class Meta:
            model = Poll
            widgets = {'content': RichTextEditorWidget}


License
-------

djboomin is licensed under a BSD 2-Clause License.
