from setuptools import setup

#test
version = '0.2.0.39'

setup(
    name='djboomin',
    version=version,
    packages=['djboomin'],
    include_package_data=True,
    author='Calm Digital',
    author_email='support@calmdigital.com',
    license='BSD 2 Clause',
    long_description='A bootstrappy django admin reskin',
    description='A bootstrappy django admin reskin',
)
